# 12306验证
- 基于Python爬虫实现12306自动登录
## 利用selenium破解验证码并自动登录
- 12306图片验证码识别，利用selenium模拟浏览器进行点击操作。
- 转至账号密码登录界面，再根据Xpath解析找到其验证码所在标签，截取验证码图片。
- 接着利用第三方超级鹰接口实现验证码正确坐标的返回，再进一步对返回坐标处理，对12306验证图片正确点击。
- 规避网站对selenium的检测，进行账号密码的填充登录
# 图片截取这里有两种方式
1. 截取全屏，获取验证码坐标，再利用PIL库根据坐标二次截取
2. 获取图片标签，直接进行二次截取，获取完整图片（此方法最为方便）

